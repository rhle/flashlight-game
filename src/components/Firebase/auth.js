import { setValue, onceValue } from './database'
import generateName from '../../name'

const firebase = window.firebase

export function onAuth (func) {
  firebase.auth().onAuthStateChanged(func)
  firebase.auth().onAuthStateChanged((user) => {
    const userNameLocation = 'users/' + user.uid + '/name'
    onceValue(userNameLocation, (snapshot) => {
      const name = snapshot.val()
      if (!name) {
        setValue(userNameLocation, generateName())
      }
    })
  })
}

export function signIn () {
  firebase.auth().signInAnonymously().catch(function (error) {
    // Handle Errors here.
    console.error(error.code)
    console.error(error.message)
    // ...
  })
}
