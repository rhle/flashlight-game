import { determineEvilSigns, getRolePool, defaultCultistsForPlayerCount, defaultInvesigatorsForPlayerCount } from './RoomInfoDisplay'
import { CULTIST, INVESTIGATOR } from './helpers'

describe('determineEvilSigns', () => {
  test('no evil signs', () => {
    expect(determineEvilSigns(false, 4)).toEqual(0)
    expect(determineEvilSigns(null, 4)).toEqual(0)
    expect(determineEvilSigns(undefined, 4)).toEqual(0)
  })

  test('use evil signs', () => {
    for (let i = 0; i < 10; i++) {
      expect(determineEvilSigns(true, Math.floor(Math.random() * 6) + 4)).toBeGreaterThan(0)
      expect(determineEvilSigns(true, Math.floor(Math.random() * 6) + 4)).toBeLessThan(4)
    }
  })
})

describe('getRolePool', () => {
  test(`right number of ${CULTIST}s and ${INVESTIGATOR}s`, () => {
    for (let i = 0; i < 10; i++) {
      const numC = Math.floor(Math.random() * 2) + 1
      const numI = Math.floor(Math.random() * 3) + 3
      const rolePool = getRolePool(numC, numI)
      expect(rolePool.filter(i => i === CULTIST).length).toEqual(numC)
      expect(rolePool.filter(i => i === INVESTIGATOR).length).toEqual(numI)
    }
  })

  test('even distribution', () => {
    for (let i = 4; i < 9; i++) {
      const numC = defaultCultistsForPlayerCount(i)
      const numI = defaultInvesigatorsForPlayerCount(i)
      const timesC = new Array(numC + numI).fill(0)
      const timesI = new Array(numC + numI).fill(0)
      for (let j = 0; j < 10000; j++) {
        getRolePool(numC, numI).forEach((value, index) => {
          if (value === CULTIST) {
            timesC[index]++
          } else {
            timesI[index]++
          }
        })
      }
      new Array(numC + numI)
        .fill(0)
        .map((_, index) => timesC[index] / (timesI[index] + timesC[index]))
        .forEach((v) => {
          expect(v).toBeCloseTo(numC / (numC + numI), 1)
        })
    }
  })
})
