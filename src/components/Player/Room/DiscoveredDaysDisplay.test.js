import React from 'react'
import { render, screen } from '@testing-library/react'
import DiscoveredDaysDisplay, { DiscoveredDayDisplay } from './DiscoveredDaysDisplay'
import renderer from 'react-test-renderer'

const numToType = jest.fn((num) => (num === 0 ? 'A' : 'B'))
const numToColor = jest.fn((num) => (num === 0 ? 'red' : 'grey'))

describe('Single Day', () => {
  beforeEach(() => {
    render(<DiscoveredDayDisplay
      cards={[0, 1, 2, 3]}
      day='day 1'
      cardNumberToType={numToType}
      cardNumberToColor={numToColor}
    />)
  })

  test('renders day heading', () => {
    expect(screen.getByText('day 1')).toBeInTheDocument()
  })

  test('renders 3 Bs', () => {
    expect(screen.getAllByText('B').length).toBe(3)
  })

  test('renders 1 A', () => {
    expect(screen.getAllByText('A').length).toBe(1)
  })
})

describe('Days Display', () => {
  // Applies only to tests in this describe block
  beforeEach(() => {
    render(<DiscoveredDaysDisplay
      days={[
        {
          day: 'day 1',
          cards: [0, 1, 2, 3]
        },
        {
          day: 'day 2',
          cards: [4, 5, 6, 7]
        }
      ]}
      cardNumberToType={numToType}
      cardNumberToColor={numToColor}
    />)
  })

  test('renders day heading', () => {
    expect(screen.getByText('day 1')).toBeInTheDocument()
    expect(screen.getByText('day 2')).toBeInTheDocument()
  })

  test('renders 3 Bs', () => {
    expect(screen.getAllByText('B').length).toBe(7)
  })

  test('renders 1 A', () => {
    expect(screen.getAllByText('A').length).toBe(1)
  })
})

describe('Empty Day Display', () => {
  test('renders nothing', () => {
    const tree = renderer.create(<DiscoveredDaysDisplay/>).toJSON()

    expect(tree).toMatchSnapshot()
  })
})
