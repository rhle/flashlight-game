import React, { Component } from 'react'
import { Container, Header, Icon, Form, Grid, Button } from 'semantic-ui-react'
import { onValueChange } from '../../Firebase/database'
import { CardDisplay } from './CardDisplay'
import { GAME_STATE } from './helpers'
import CardTypeCountDisplay from './CardTypeCountDisplay'
import PropTypes from 'prop-types'
class PlayerHandDisplayBase extends Component {
  constructor (props) {
    super(props)
    const { userId } = props
    this.state = {
      displayName: userId,
      editedCthulhus: 0,
      editedSigns: 0,
      editedEvilSigns: 0
    }
  }

  componentDidMount () {
    onValueChange('users/' + this.props.playerId + '/name', (snapshot) => {
      const v = snapshot.val()
      if (v) {
        this.setState({ displayName: v })
      }
    })
    onValueChange('rooms/' + this.props.roomCode + '/players/' + this.props.playerId + '/claims', (snapshot) => {
      const v = snapshot.val()
      this.setState({
        editedCthulhus: v.cthulhus,
        editedSigns: v.signs,
        editedEvilSigns: v.evilSigns
      })
    })
  }

  render () {
    if ([this.props.playerId, this.props.playerData, this.props.userId].some((item) => item === null || item === undefined)) {
      return (<Container></Container>)
    }

    if ([this.props.playerData.hand].some((item) => item === null || item === undefined)) {
      return (<Container></Container>)
    }

    const disableEditDeclare = !this.props.canDeclare

    return (<Grid.Column tablet='6' computer='5' largeScreen='4' widescreen='4'>
      <Container textAlign='center'>

        {this.handHeader()}
        <Container>
          <Form>
            <CardTypeCountDisplay
              cthulhus={this.props.playerData.claims.cthulhus}
              signs={this.props.playerData.claims.signs}
              evilSigns={this.props.playerData.claims.evilSigns}
              isEvilSigns={this.props.isEvilSigns}
              hideIfZero={this.props.hasClaimed} />
            {this.declareInputFields(disableEditDeclare)}

            <Form.Group widths='equal'>
              {this.declareButton(disableEditDeclare)}
            </Form.Group>
          </Form>
        </Container>

        <Container>
          <Button.Group centered='true' fluid size='mini' compact>
            {this.turnHandIntoCardDisplays()}
          </Button.Group>
        </Container>

      </Container>
    </Grid.Column>)
  }

  turnHandIntoCardDisplays () {
    return this.props.playerData.hand
      .map((cardNumber) => (
        <CardDisplay
          cardNumber={cardNumber}
          key={cardNumber}
          cardOnMouseEnter={this.props.cardOnMouseEnter}
          cardOnMouseLeave={this.props.cardOnMouseLeave}
          cardColor={this.props.cardNumberToColor(cardNumber)}
          cardOnClick={this.props.cardOnClick}
          cardDisplayValue={(
            window.location.hostname.indexOf('gitpod') !== -1 ||
            (this.props.discoveredCards && this.props.discoveredCards.indexOf(cardNumber) >= 0)
              ? this.props.cardNumberToValue(cardNumber)
              : '?')
          } />))
  }

  handHeader () {
    const sortedCards = this.props.playerData.hand.slice()
    sortedCards.sort((a, b) => Number(a) - Number(b))
    return (<Header as='h2' icon textAlign='center'>
      <Icon name={this.props.playerIcon} color={this.props.playerColor} circular />
      <Header.Content>{this.state.displayName} {(this.props.playerId === this.props.userId ? ' (You)' : '')}</Header.Content>
      <PlayerInformation
        sortedCards={sortedCards}
        playerId={this.props.playerId}
        userId={this.props.userId}
        playerRole={this.props.playerData.role}
        cardNumberToValue={this.props.cardNumberToValue}
        gameState={this.props.gameState}
      />
    </Header>)
  }

  declareButton (disableEditDeclare) {
    return (this.props.playerId === this.props.userId
      ? (<Form.Button
        disabled={disableEditDeclare}
        onClick={() => { this.props.onDeclare(this.props.playerId, this.state.editedCthulhus, this.state.editedSigns, this.state.editedEvilSigns) } }>
                        Declare
      </Form.Button>)
      : (<div></div>))
  }

  onUpdateClaim (type, value) {
    const update = {}
    update['edited' + type.replace(' ', '')] = (Number(value) > -1 && Number(value) < 6 ? Number(value) : 0)
    this.setState(update)
  }

  declareInputField (label, disabled, showWhen = true) {
    if (showWhen === false) {
      return <React.Fragment/>
    }

    const noSpace = label.replace(' ', '')
    const editedString = 'edited' + noSpace
    const claimsString = noSpace[0].toLowerCase() + noSpace.slice(1)

    let color

    switch (label) {
      case 'Cthulhus':
        color = 'red'
        break
      case 'Signs':
        color = 'green'
        break
      default:
        color = 'orange'
    }

    const fieldVal = (Number.isInteger(this.state[editedString]) ? this.state[editedString] : this.props.playerData.claims[claimsString])
    const fieldStr = fieldVal + ' ' + label.charAt(0)
    return (
      <Button.Group compact size='mini'>
        <Button disabled={disabled} onClick={() => this.onUpdateClaim(label, fieldVal - 1)}>&lt;</Button>
        <Button color={color} disabled={true}>{fieldStr}</Button>
        <Button disabled={disabled} onClick={() => this.onUpdateClaim(label, fieldVal + 1)}>&gt;</Button>
      </Button.Group>
    )

    /*
    return (
      <Form.Input
        fluid
        type={'number'}
        placeholder={label}
        label={label}
        size='mini'
        disabled={disabled}
        value={(Number.isInteger(this.state[editedString]) ? this.state[editedString] : this.props.playerData.claims[claimsString])}
        onChange={(_, data) => this.onUpdateClaim(label, data.value)} />
    ) */
  }

  declareInputFields (disableEditDeclare) {
    if (
      this.props.playerId === this.props.userId
    ) {
      return (
        <React.Fragment>
          {this.declareInputField('Cthulhus', disableEditDeclare)}
          {this.declareInputField('Signs', disableEditDeclare)}
          {this.declareInputField('Evil Signs', disableEditDeclare, this.props.isEvilSigns)}
        </React.Fragment>
      )
    } else {
      return (
        <React.Fragment/>
      )
    }
  }
}

PlayerHandDisplayBase.propTypes = {
  userId: PropTypes.string,
  playerId: PropTypes.string,
  playerData: PropTypes.shape(
    {
      hand: PropTypes.arrayOf(PropTypes.number),
      role: PropTypes.string,
      claims: PropTypes.shape({
        signs: PropTypes.number,
        cthulhus: PropTypes.number,
        evilSigns: PropTypes.number
      })
    }
  ),
  cardOnMouseEnter: PropTypes.func,
  cardOnMouseLeave: PropTypes.func,
  cardOnClick: PropTypes.func,
  activeCard: PropTypes.number,
  discoveredCards: PropTypes.arrayOf(PropTypes.number),
  cardNumberToValue: PropTypes.func,
  numPlayers: PropTypes.number,
  canDeclare: PropTypes.bool,
  onDeclare: PropTypes.func,
  playerIcon: PropTypes.string,
  playerColor: PropTypes.string,
  cardNumberToColor: PropTypes.func,
  gameState: PropTypes.string,
  roomCode: PropTypes.string,
  isEvilSigns: PropTypes.bool,
  hasClaimed: PropTypes.bool
}

export function PlayerInformation ({ sortedCards, playerId, userId, playerRole, cardNumberToValue, gameState }) {
  if (playerId === userId || gameState === GAME_STATE.END) {
    return (<Container>
      <Header sub>
        {playerRole}
      </Header>
      <Header as='h3' sub>
        {sortedCards.map((cardNumber) => cardNumberToValue(cardNumber)).join(' ')}
      </Header>
    </Container>)
  }
  return (<Container></Container>)
}

PlayerInformation.propTypes = {
  sortedCards: PropTypes.arrayOf(PropTypes.number),
  playerId: PropTypes.string,
  userId: PropTypes.string,
  playerRole: PropTypes.string,
  cardNumberToValue: PropTypes.func,
  gameState: PropTypes.string
}

const PlayerHandDisplay = PlayerHandDisplayBase
export default PlayerHandDisplay
