import {
  cardNumberToType,
  typesDiscovered,
  checkWinner,
  CARD_CTHULHU,
  CARD_SIGN,
  CARD_NOTHING,
  CARD_EVIL,
  CULTIST,
  INVESTIGATOR
} from './helpers'

describe('cardNumberToType', () => {
  test(CARD_CTHULHU, () => {
    expect(cardNumberToType(0, 1)).toEqual(CARD_CTHULHU)
    expect(cardNumberToType(0, 2)).toEqual(CARD_CTHULHU)
    expect(cardNumberToType(0, 3)).toEqual(CARD_CTHULHU)
    expect(cardNumberToType(0, 4)).toEqual(CARD_CTHULHU)
  })

  test(CARD_EVIL, () => {
    expect(cardNumberToType(1, 1, 1)).toEqual(CARD_EVIL)
    expect(cardNumberToType(1, 2, 1)).toEqual(CARD_EVIL)
    expect(cardNumberToType(2, 3, 1)).toEqual(CARD_SIGN)
    expect(cardNumberToType(2, 3, 2)).toEqual(CARD_EVIL)
    expect(cardNumberToType(3, 4, 1)).toEqual(CARD_SIGN)
    expect(cardNumberToType(4, 4, 1)).toEqual(CARD_SIGN)
  })

  test(CARD_SIGN, () => {
    expect(cardNumberToType(1, 1)).toEqual(CARD_SIGN)
    expect(cardNumberToType(1, 2)).toEqual(CARD_SIGN)
    expect(cardNumberToType(2, 3)).toEqual(CARD_SIGN)
    expect(cardNumberToType(3, 4)).toEqual(CARD_SIGN)
    expect(cardNumberToType(4, 4)).toEqual(CARD_SIGN)
  })

  test(CARD_NOTHING, () => {
    expect(cardNumberToType(2, 1)).toEqual(CARD_NOTHING)
    expect(cardNumberToType(3, 2)).toEqual(CARD_NOTHING)
    expect(cardNumberToType(12, 3)).toEqual(CARD_NOTHING)
    expect(cardNumberToType(5, 4)).toEqual(CARD_NOTHING)
    expect(cardNumberToType(14, 4)).toEqual(CARD_NOTHING)
  })
})

describe('typesDiscovered', () => {
  test('finds ' + CARD_CTHULHU, () => {
    expect(typesDiscovered([0, 1], 1).cthulhus).toEqual(1)
    expect(typesDiscovered([1, 0], 1).cthulhus).toEqual(1)
    expect(typesDiscovered([], 1).cthulhus).toEqual(0)
  })

  test('finds ' + CARD_SIGN, () => {
    expect(typesDiscovered([1, 2, 3, 4], 4).signs).toEqual(4)
    expect(typesDiscovered([], 4).signs).toEqual(0)
    expect(typesDiscovered([8, 14, 21, 9], 8).signs).toEqual(1)
    expect(typesDiscovered([15, 21, 13], 4).signs).toEqual(0)
  })

  test('finds ' + CARD_EVIL, () => {
    expect(typesDiscovered([1, 2, 3, 4], 4, 2).evilSigns).toEqual(2)
    expect(typesDiscovered([], 4, 2).evilSigns).toEqual(0)
    expect(typesDiscovered([8, 14, 21, 3], 8, 3).signs).toEqual(1)
  })
})

describe('checkWinner', () => {
  test(CULTIST + ' wins by ' + CARD_CTHULHU, () => {
    expect(checkWinner([0], 1)).toEqual(CULTIST)
    expect(checkWinner([10, 20, 13, 8, 0, 1], 2)).toEqual(CULTIST)
  })

  test(CULTIST + ' wins by timeout', () => {
    for (let i = 4; i <= 8; i++) {
      expect(
        checkWinner(Array(i * 4).fill(20), i)
      ).toEqual(CULTIST)
    }
  })

  test(CULTIST + 'wins by ' + CARD_EVIL, () => {
    expect(checkWinner([10, 7, 1], 4, 1)).toEqual(CULTIST)
    expect(checkWinner([10, 7, 14, 3], 4, 3)).toEqual(CULTIST)
  })

  test(INVESTIGATOR + ' wins', () => {
    expect(checkWinner([1, 2, 3, 4], 4)).toEqual(INVESTIGATOR)
    expect(checkWinner([1, 2, 3, 4, 5, 6, 7, 8], 4)).toEqual(INVESTIGATOR)
    expect(checkWinner([1, 2, 3, 4], 4, 1)).toEqual(INVESTIGATOR)
    expect(checkWinner([1, 2, 3, 4, 5, 6, 7, 8], 4, 2)).toEqual(INVESTIGATOR)
  })

  test('game is not over', () => {
    expect(checkWinner([1, 2, 3], 4)).toEqual('')
    expect(checkWinner([1, 2, 4, 5, 6, 7, 8], 4)).toEqual('')
    expect(checkWinner([10, 7, 14, 4, 3], 4, 3)).toEqual('')
  })
})
