import React from 'react'
import { Container, Label } from 'semantic-ui-react'
import PropTypes from 'prop-types'

export default function CardTypeCountDisplay ({ cthulhus, signs, isEvilSigns, evilSigns, hideIfZero = false, evilSignNote = '' }) {
  return (
    <Container textAlign='center'>
      <CardLabel label='Cthulhus' color='red' number={cthulhus} showWhen={!hideIfZero || cthulhus > 0}/>
      {(isEvilSigns && evilSignNote ? <br/> : '')}
      <CardLabel label='Signs' color='green' number={signs} showWhen={!hideIfZero || signs > 0 }/>
      {(isEvilSigns && evilSignNote ? evilSignNote : '')}
      <CardLabel label='Evils' color='orange' number={evilSigns} showWhen={isEvilSigns && (!hideIfZero || evilSigns > 0)} />
    </Container>
  )
}

function CardLabel ({ label, color, number, showWhen = true }) {
  if (!showWhen) {
    return (<React.Fragment/>)
  }
  return (
    <Label color={(number === 0 ? '' : color)}>
      {label}
      <Label.Detail>{number}</Label.Detail>
    </Label>
  )
}

CardLabel.propTypes = {
  label: PropTypes.string,
  color: PropTypes.string,
  number: PropTypes.number,
  showWhen: PropTypes.bool
}

CardTypeCountDisplay.propTypes = {
  cthulhus: PropTypes.number,
  signs: PropTypes.number,
  isEvilSigns: PropTypes.bool,
  evilSigns: PropTypes.number,
  hideIfZero: PropTypes.bool,
  evilSignNote: PropTypes.string
}
