import React from 'react'
import { Container, Button } from 'semantic-ui-react'
import PropTypes from 'prop-types'

export function CardDisplay (props) {
  const { cardNumber, cardOnMouseEnter, cardOnMouseLeave, cardColor, cardOnClick, cardDisplayValue } = props
  if ([cardNumber].some((item) => item === null || item === undefined)) {
    return (<Container></Container>)
  }
  return (<Button
    onMouseEnter={() => cardOnMouseEnter(cardNumber)}
    onMouseLeave={() => cardOnMouseLeave(cardNumber)}
    onClick={() => cardOnClick(cardNumber)}
    color={cardColor}>
    {cardDisplayValue}
  </Button>)
}

CardDisplay.propTypes = {
  cardNumber: PropTypes.number.isRequired,
  cardOnMouseEnter: PropTypes.func,
  cardOnMouseLeave: PropTypes.func,
  cardColor: PropTypes.string,
  cardOnClick: PropTypes.func,
  cardDisplayValue: PropTypes.string
}
