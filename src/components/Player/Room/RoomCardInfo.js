import React from 'react'
import { Card, Header } from 'semantic-ui-react'
import { typesDiscovered, GAME_STATE } from './helpers'
import CardTypeCountDisplay from './CardTypeCountDisplay'
import PropTypes from 'prop-types'

export default function RoomCardInfo ({ numPlayers, discoveredCards = [], claims = [], gameState = GAME_STATE.LOBBY, totalEvilSigns = 0 }) {
  const { cthulhus, signs, evilSigns } = typesDiscovered(discoveredCards, numPlayers, totalEvilSigns, false)
  const isEvilSigns = totalEvilSigns > 0

  if (gameState !== GAME_STATE.LOBBY) {
    return (
      <Card fluid>
        <Card.Content>
          <Card.Header>Game Info</Card.Header>
        </Card.Content>
        <Card.Content>
          <Header as='h4'>Cards in Game</Header>
          <CardTypeCountDisplay
            cthulhus={1}
            signs={numPlayers}
            isEvilSigns={isEvilSigns}
            evilSignNote='Including max'
            evilSigns={
              (numPlayers === 4 ? 2 : 3)
            }/>
        </Card.Content>
        <Card.Content>
          <Header as='h4'>Discovered Cards</Header>
          <CardTypeCountDisplay cthulhus={cthulhus} signs={signs} isEvilSigns={isEvilSigns} evilSignNote='Including' evilSigns={evilSigns}/>
        </Card.Content>
        <Card.Content>
          <Header as='h4'>Claimed Cards</Header>
          <CardTypeCountDisplay
            cthulhus={claims.map((i) => i.cthulhus).reduce((a, c) => a + c, 0)}
            signs={claims.map((i) => i.signs + i.evilSigns).reduce((a, c) => a + c, 0)}
            isEvilSigns={isEvilSigns}
            evilSignNote='Including'
            evilSigns={claims.map((i) => i.evilSigns).reduce((a, c) => a + c, 0)}
          />
        </Card.Content>
      </Card>
    )
  } else {
    return (
      <React.Fragment/>
    )
  }
}

RoomCardInfo.propTypes = {
  numPlayers: PropTypes.number,
  discoveredCards: PropTypes.array,
  claims: PropTypes.array,
  gameState: PropTypes.string,
  evilSigns: PropTypes.number
}
