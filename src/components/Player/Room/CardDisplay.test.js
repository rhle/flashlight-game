import React from 'react'
import { render, screen } from '@testing-library/react'
import { CardDisplay } from './CardDisplay'
import userEvent from '@testing-library/user-event'

const cardValue = 0
let displayValue = '?'
const onMouseEnter = jest.fn()
const onMouseLeave = jest.fn()
const onClick = jest.fn((cardNumber) => {
  displayValue = cardNumber
})

describe('Card Display', () => {
  // Applies only to tests in this describe block
  beforeEach(() => {
    render(<CardDisplay
      cardNumber={cardValue}
      cardOnMouseEnter={onMouseEnter}
      cardOnMouseLeave={onMouseLeave}
      cardColor='green'
      cardOnClick={onClick}
      cardDisplayValue={displayValue}
    />)
  })

  test('renders', () => {
    expect(screen.getByText(displayValue)).toBeInTheDocument()
  })

  test('responds to mouseover', () => {
    userEvent.hover(screen.getByText(displayValue))
    expect(onMouseEnter.mock.calls[0][0]).toBe(cardValue)
    userEvent.unhover(screen.getByText(displayValue))
    expect(onMouseLeave.mock.calls[0][0]).toBe(cardValue)
  })

  test('changes display on click', () => {
    userEvent.click(screen.getByText(displayValue))
    expect(onClick.mock.calls[0][0]).toBe(cardValue)
  })
})
