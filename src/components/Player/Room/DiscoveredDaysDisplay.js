import React from 'react'
import { Container, Segment, Grid, Header, Card } from 'semantic-ui-react'
import { CardDisplay } from './CardDisplay'
import PropTypes from 'prop-types'

export function DiscoveredDaysDisplay (props) {
  if (props.days) {
    const days = props.days.map(
      (dayInfo) =>
        <Grid.Column key={dayInfo.day}>
          <DiscoveredDayDisplay
            cardNumberToType={props.cardNumberToType}
            day={dayInfo.day}
            cards={dayInfo.cards}
            cardNumberToColor={props.cardNumberToColor}
          />
        </Grid.Column>)
    return (<Grid container stackable columns={4}>
      {days}
    </Grid>)
  }
  return (<Container></Container>)
}

DiscoveredDaysDisplay.propTypes = {
  days: PropTypes.arrayOf(
    PropTypes.shape({
      day: PropTypes.string,
      cards: PropTypes.arrayOf(PropTypes.number)
    })
  ),
  cardNumberToType: PropTypes.func,
  cardNumberToColor: PropTypes.func
}

export function DiscoveredDayDisplay (props) {
  const cards = props.cards.map(
    (number) =>
      <CardDisplay
        cardOnMouseEnter={() => { }}
        cardNumber={number}
        cardOnMouseLeave={() => { }}
        cardOnClick={() => { }}
        cardDisplayValue={props.cardNumberToType(number)}
        cardColor={props.cardNumberToColor(number)}
        key={number}
      />)
  return (<Segment textAlign='center'>
    <Header as='h2' textAlign='center'>
      <Header.Content>{props.day}</Header.Content>
    </Header>
    <Card.Group centered>
      {cards}
    </Card.Group>
  </Segment>)
}

DiscoveredDayDisplay.propTypes = {
  cards: PropTypes.arrayOf(PropTypes.number),
  day: PropTypes.string,
  cardNumberToType: PropTypes.func,
  cardNumberToColor: PropTypes.func
}

export default DiscoveredDaysDisplay
