import { GAME_STATE, INVESTIGATOR, CULTIST } from '../helpers'
import { pushValue, setValue } from '../../Firebase/database'

export const CARD_CTHULHU = 'C'
export const CARD_SIGN = 'S'
export const CARD_EVIL = 'E'
export const CARD_NOTHING = 'N'
export const SYSTEM_USER = 'SYSTEM'

export { GAME_STATE, INVESTIGATOR, CULTIST }

export function cardNumberToType (cardNumber, numPlayers, numEvilSigns = 0) {
  if (Number(cardNumber) === 0) {
    return CARD_CTHULHU
  }
  if (Number(cardNumber) <= Number(numEvilSigns)) {
    return CARD_EVIL
  }
  if (Number(cardNumber) <= Number(numPlayers)) {
    return CARD_SIGN
  }
  return CARD_NOTHING
}
export function shuffle (arr) {
  const array = arr.slice(0)
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1))
    const temp = array[i]
    array[i] = array[j]
    array[j] = temp
  }
  return array
}

export function typesDiscovered (cards, numPlayers, numEvilSigns = 0, seperateSigns = true) {
  const importantCards = cards.filter((i) => {
    return (i <= numPlayers)
  })
  return {
    cthulhus: importantCards.filter((i) => i === 0).length,
    evilSigns: importantCards.filter((i) => i !== 0 && i <= numEvilSigns).length,
    signs: importantCards.filter((i) => i !== 0 && (!seperateSigns || i > numEvilSigns)).length
  }
}
export function checkWinner (cards, numPlayers, numEvilSigns = 0) {
  const { cthulhus, signs, evilSigns } = typesDiscovered(cards, numPlayers, numEvilSigns, true)
  if (cthulhus > 0) {
    return CULTIST
  }
  if (evilSigns > 0 && signs === 0) {
    return CULTIST
  }
  if (signs + evilSigns === numPlayers) {
    return INVESTIGATOR
  }
  if (cards.length >= numPlayers * 4) {
    return CULTIST
  }
  return ''
}

export function addHistory (room, actor, action, data, day) {
  return pushValue('/rooms/' + room + '/history', {
    user: actor,
    action: action,
    data: data,
    timestamp: new Date().getTime(),
    day: day
  })
}

export function startDay (thisDay, playerOrder, discoveredCards = [], roomCode) {
  const numPlayers = playerOrder.length
  let deck = Array(numPlayers * 5).fill('N').map((_, i) => i)
  deck = deck.filter((value) => discoveredCards.indexOf(value) === -1)
  deck = shuffle(deck)
  const handSize = deck.length / numPlayers
  const hands = playerOrder.map((_) => Array(Math.floor(handSize)).fill(1).map(() => deck.pop()))
  const playerHandMap = {}

  playerOrder.forEach((userId, index) => {
    playerHandMap[userId] = hands[index]
    setValue('/rooms/' + roomCode + '/players/' + userId + '/hand', hands[index])
    setValue('/rooms/' + roomCode + '/players/' + userId + '/claims', {
      cthulhus: 0,
      signs: 0,
      evilSigns: 0
    })
  })
  setValue('/rooms/' + roomCode + '/activeCard', '')
  setValue('/rooms/' + roomCode + '/declaredPlayers', '')
  setValue('/rooms/' + roomCode + '/day', thisDay)
  setValue('/rooms/' + roomCode + '/numDeclares', 0)
  setValue('/rooms/' + roomCode + '/status', GAME_STATE.DECLARE)
  addHistory(roomCode, SYSTEM_USER, 'BEGIN DAY', {
    day: thisDay,
    hands: playerHandMap
  }, thisDay)
}
