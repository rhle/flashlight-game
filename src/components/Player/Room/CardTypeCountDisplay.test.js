import React from 'react'
import { render, screen } from '@testing-library/react'
import CardTypeCountDisplay from './CardTypeCountDisplay'

describe('Card Count Display', () => {
  test('renders', () => {
    render(<CardTypeCountDisplay
      cthulhus={1}
      signs={4}
    />)
    expect(screen.getByText('Cthulhus')).toBeInTheDocument()
    expect(screen.getByText('Signs')).toBeInTheDocument()
    expect(screen.getByText('1')).toBeInTheDocument()
    expect(screen.getByText('4')).toBeInTheDocument()
  })

  test('shows evil signs', () => {
    render(<CardTypeCountDisplay
      cthulhus={0}
      signs={0}
      isEvilSigns={true}
      evilSigns={0}

    />)
    expect(screen.getByText('Evils')).toBeInTheDocument()
  })

  test('hide evil signs', () => {
    render(<CardTypeCountDisplay
      cthulhus={0}
      signs={0}
      isEvilSigns={false}
      evilSigns={0}

    />)
    expect(screen.queryByText('Evils')).not.toBeInTheDocument()
  })

  test('hideIfZero', () => {
    render(<CardTypeCountDisplay
      cthulhus={0}
      signs={0}
      isEvilSigns={true}
      evilSigns={0}
      hideIfZero={true}
    />)
    expect(screen.queryByText('Cthulhus')).not.toBeInTheDocument()
    expect(screen.queryByText('Signs')).not.toBeInTheDocument()
    expect(screen.queryByText('Evils')).not.toBeInTheDocument()
  })
})
