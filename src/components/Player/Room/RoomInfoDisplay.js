import React, { Component } from 'react'
import { Container, Card, Header, Icon, Segment, Button, Form } from 'semantic-ui-react'
import { shuffle, CULTIST, INVESTIGATOR, startDay } from './helpers'
import { onValueChange, setValue } from '../../Firebase/database'
import { joinRoom } from '../helpers'
import PropTypes from 'prop-types'

export function determineEvilSigns (isEvilSigns, numPlayers) {
  if (!isEvilSigns) {
    return 0
  }
  let signPool = new Array(Math.max(numPlayers, 6)).fill(false).concat(new Array(2).fill(true))
  signPool = shuffle(signPool)
  const baseSigns = [false, false, true]
  const additionalSigns = new Array(numPlayers - 3).fill(false).map(() => signPool.pop())
  return baseSigns.concat(additionalSigns).filter((i) => i).length
}

export function getRolePool (numCultists, numInvestigators) {
  return shuffle(
    new Array(numCultists)
      .fill(CULTIST)
      .concat(
        new Array(numInvestigators).fill(INVESTIGATOR)
      )
  )
}

export function defaultCultistsForPlayerCount (playerCount) {
  if (playerCount > 6) {
    return 3
  }

  return 2
}

export function defaultInvesigatorsForPlayerCount (playerCount) {
  if (playerCount > 7) {
    return 6
  }
  if (playerCount === 7) {
    return 5
  }
  if (playerCount > 4) {
    return 4
  }
  return 3
}

class RoomInfoDisplay extends Component {
  constructor (props) {
    super(props)

    this.state = {
      cultists: 0,
      investigators: 0,
      isEvilSigns: false,
      playerCount: (this.props.roomData !== null ? Object.keys(this.props.roomData.users).length : 0)
    }
  }

  componentDidMount () {
    onValueChange('rooms/' + this.props.roomCode + '/users', (snapshot) => {
      if (snapshot.val()) {
        const playerCount = Object.keys(snapshot.val()).length
        this.setState(
          {
            playerCount: playerCount,
            cultists: defaultCultistsForPlayerCount(playerCount),
            investigators: defaultInvesigatorsForPlayerCount(playerCount)
          }
        )
      }
    })
  }

  basicRoomSettings (users, players) {
    const roomDataMap = new Map()
    roomDataMap.set('day', 1)
    roomDataMap.set('day1', '')
    roomDataMap.set('day2', '')
    roomDataMap.set('day3', '')
    roomDataMap.set('day4', '')
    roomDataMap.set('history', [])
    roomDataMap.set('evilSigns', determineEvilSigns(this.state.isEvilSigns, users.length))
    roomDataMap.set('players', players)
    roomDataMap.set('playerOrder', users)
    roomDataMap.set('currentPlayer', users[0])
    return roomDataMap
  }

  setupPlayers (users) {
    const players = {}
    const rolePool = getRolePool(this.state.cultists, this.state.investigators)
    users.forEach((userId) => {
      players[userId] = {
        role: rolePool.pop(),
        claims: {
          cthulhus: 0,
          signs: 0
        }
      }
    })
    return players
  }

  initGame () {
    if (this.props.roomCode == null || this.props.roomCode === undefined || this.props.roomData === null || this.props.roomData === undefined) {
      return
    }
    const prefix = 'rooms/' + this.props.roomCode
    setValue(prefix + '/status', 'loading')
    setValue(prefix + '/declaredPlayers', '')
    const users = Object.keys(this.props.roomData.users)
    setValue(prefix, this.basicRoomSettings(shuffle(users), this.setupPlayers(users)), false)
    startDay(1, users, [], this.props.roomCode)
    setValue(prefix + '/status', 'declare')
  }

  updateCultists (_, data) {
    this.setState({ cultists: Number(data.value) })
  }

  updateInvestigators (_, data) {
    this.setState({ investigators: Number(data.value) })
  }

  updateEvilSigns (_, data) {
    this.setState({ isEvilSigns: !this.state.isEvilSigns })
  }

  render () {
    if (this.props.roomData === null) {
      return (<Container></Container>)
    }
    const players = Object.keys(this.props.roomData.users)
    const isPlayerMember = players.includes(this.props.userId)
    const playerCount = (this.props.roomData !== null ? players.length : 0)
    const isRoomOwner = this.props.userId === this.props.roomData.owner
    const startDisabled = playerCount < 4 || playerCount > (this.state.investigators + this.state.cultists)
    const startButton = (isRoomOwner ? (
      <Button disabled={startDisabled} primary
        onClick={this.initGame.bind(this)}
        key='startButton'>
        Start Game
      </Button>) : null)
    const joinRoomButton = (isRoomOwner || isPlayerMember ? null : (<Button primary key='joinRoomButton' onClick={() => joinRoom(this.props.roomCode, this.props.userId)}>Join Room</Button>))
    if (startButton === null && joinRoomButton === null) {
      return <React.Fragment/>
    }
    return (<Segment textAlign='center'>
      <Header as='h2' icon textAlign='center'>
        <Icon name='settings' circular />
        <Header.Content>Settings</Header.Content>
      </Header>
      <Card.Group centered>
        <Card>
          <Card.Content>
            <Card.Header>Room Code</Card.Header>
            {this.props.roomCode}
          </Card.Content>
        </Card>
        <GameRoomSettings
          cultists={this.state.cultists}
          investigators={this.state.investigators}
          isEvilSigns={this.state.isEvilSigns}
          updateEvilSigns={this.updateEvilSigns.bind(this)}
          updateCultists={this.updateCultists.bind(this)}
          updateInvestigators={this.updateInvestigators.bind(this)}
          showWhen={isRoomOwner}
        />
        <Card>
          <Card.Content>
            {[startButton, joinRoomButton]}
          </Card.Content>
        </Card>
      </Card.Group>

    </Segment>)
  }
}

function GameRoomSettings ({ cultists, investigators, updateCultists, updateInvestigators, isEvilSigns, updateEvilSigns, showWhen }) {
  if (showWhen) {
    return (
      <Card>
        <Card.Content>
          <Card.Header>New Game Settings</Card.Header>
          <Form>
            <Form.Group inline widths='equal'>
              <Form.Input
                fluid
                type={'number'}
                placeholder='Cultists'
                label='Cultists'
                value={cultists}
                onChange={updateCultists} />
              <Form.Input
                fluid
                type={'number'}
                placeholder='Investigators'
                label='Investigators'
                value={investigators}
                onChange={updateInvestigators} />
            </Form.Group>
            <Form.Group inline widths='equal'>
              <Form.Checkbox
                fluid
                toggle
                checked={isEvilSigns}
                onChange={updateEvilSigns}
                label='Evil Signs'/>
            </Form.Group>
          </Form>
        </Card.Content>
      </Card>

    )
  } else {
    return (
      <React.Fragment></React.Fragment>
    )
  }
}

RoomInfoDisplay.propTypes = {
  roomData: PropTypes.object,
  userId: PropTypes.string,
  roomCode: PropTypes.string
}

export default RoomInfoDisplay
