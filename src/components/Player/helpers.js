import { onceValue, setValue } from '../Firebase/database'

export const GAME_STATE = {
  LOBBY: 'lobby',
  DECLARE: 'declare',
  PLAYING: 'playing',
  PENDING: 'pending',
  END: 'end'
}

export const INVESTIGATOR = 'Investigator'
export const CULTIST = 'Cultist'

export function joinRoom (roomCode, user, onSuccess = () => {}, onFailure = () => {}) {
  if (roomCode) {
    onceValue('rooms/' + roomCode, (snapshot) => {
      const roomData = snapshot.val()
      if (roomData) {
        setValue('rooms/' + roomCode + '/users/' + user, true)
        setValue('users/' + user + '/activeGame', roomCode)
        onSuccess(true)
      } else {
        onFailure('Invalid Room Code')
      }
    })
  } else {
    onFailure('No Room Code')
  }
}

export function requestNotificationPermission () {
  if (!window.Notification) {
    console.log('Browser does not support notifications.')
  } else {
    if (Notification.permission === 'granted') {

    } else {
      return Notification.requestPermission().then(function (p) {
        if (p === 'granted') {
          // show notification here
          console.log('Notifications allowed')
        } else {
          console.log('User blocked notifications.')
        }
      }).catch(function (err) {
        console.error(err)
      })
    }
  }
}

export function sendNotification (message) {
  let n
  if (!window.Notification) {
    console.log('Browser does not support notifications.')
  } else {
    if (Notification.permission === 'granted') {
      n = new Notification(message)
    }
  }

  if (n) {
    console.log('Notification sent')
  }
}
