import React, { Component } from 'react'
import { Container, Card, Header } from 'semantic-ui-react'
import { INVESTIGATOR } from '../helpers'
import { onValueChange, onceValue } from '../../Firebase/database'
import PropTypes from 'prop-types'
import { formatDistanceToNow } from 'date-fns'

export default class Archive extends Component {
  constructor (props) {
    super(props)

    this.state = {
      archivedGames: []
    }
  }

  componentDidMount () {
    onValueChange('/users/' + this.props.userId + '/archive', (snapshot) => {
      if (snapshot.val()) {
        this.setState({
          archivedGames: snapshot.val()
        })
      }
    })
  }

  render () {
    return (
      <Container>
        <Header>Archived Games</Header>
        <p>
          {this.state.archivedGames.map((archiveId) => (<ArchiveRoomPreview archiveId={archiveId} userId={this.props.userId} key={archiveId}/>))}
        </p>
      </Container>
    )
  }
}

Archive.propTypes = {
  userId: PropTypes.string
}

class ArchiveRoomPreview extends Component {
  constructor (props) {
    super(props)

    this.state = {
      roomData: null,
      winnerString: null
    }
  }

  componentDidMount () {
    onceValue('/history/' + this.props.archiveId).then((snapshot) => {
      const roomData = snapshot.val()
      const history = Object.values(roomData.history)
      history.reverse()
      const role = roomData.players[this.props.userId].role
      // console.log(history[0])
      this.setState({
        roomData: roomData,
        role: role,
        win: history[0].data.winner.indexOf(role) === 0,
        timestamp: history[0].timestamp
      })
    })
  }

  render () {
    if (this.state.roomData) {
      return (
        <Container>
          <Card onClick={() => { window.location.pathname = '/archive/' + this.props.archiveId }}>
            <Card.Content>
              <Card.Header>{(this.state.win ? 'Win!' : 'Loss')}</Card.Header>
              <Card.Meta>
                <span className='date'>{formatDistanceToNow(new Date(this.state.timestamp), { includeSeconds: true, addSuffix: true })}</span>
              </Card.Meta>
              You were a{(this.state.role === INVESTIGATOR ? 'n' : '')} {this.state.role}
            </Card.Content>
          </Card>
        </Container>
      )
    }

    return (
      <React.Fragment/>
    )
  }
}

ArchiveRoomPreview.propTypes = {
  archiveId: PropTypes.string,
  userId: PropTypes.string
}
