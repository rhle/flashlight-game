import React from 'react'
import { render, screen, fireEvent } from '@testing-library/react'
import SplashScreen, { NewRoomCard, JoinRoomCard } from './index'
import userEvent from '@testing-library/user-event'

test('renders cards and links', () => {
  render(<SplashScreen />)
  expect(screen.getByText(/create room/i)).toBeInTheDocument()
  expect(screen.getByText(/join room/i)).toBeInTheDocument()
})

describe('JoinRoomCard', () => {
  test('renders', () => {
    render(<JoinRoomCard/>)
    expect(screen.getByText(/join room/i)).toBeInTheDocument()
    expect(screen.getByPlaceholderText(/room code/i)).toBeInTheDocument()
  })

  test('runs joinRoomOnClick onClick', () => {
    const mockJoinRoomOnClick = jest.fn()
    render(<JoinRoomCard joinRoomOnClick={mockJoinRoomOnClick} />)
    expect(mockJoinRoomOnClick.mock.calls.length).toBe(0)
    fireEvent.click(screen.getByRole('button'))
    expect(mockJoinRoomOnClick.mock.calls.length).toBe(1)
  })

  test('runs updateRoomCode', () => {
    const mockUpdateCode = jest.fn()
    render(<JoinRoomCard updateRoomCode={mockUpdateCode} />)
    expect(mockUpdateCode.mock.calls.length).toBe(0)
    userEvent.type(screen.getByPlaceholderText(/room code/i), '123456')
    expect(mockUpdateCode.mock.calls.length).toBe(6)
  })

  test('shows roomCode', () => {
    render(<JoinRoomCard roomCode={'123456'} />)
    expect(screen.getByDisplayValue(/123456/i)).toBeInTheDocument()
  })
})

describe('NewRoomCard', () => {
  test('renders', () => {
    render(<NewRoomCard/>)
    expect(screen.getByText(/create room/i)).toBeInTheDocument()
    expect(screen.getByText(/create a new room/i)).toBeInTheDocument()
  })

  test('runs newRoomWrite onClick', () => {
    const mockNewRoomWrite = jest.fn()
    render(<NewRoomCard newRoomWrite={mockNewRoomWrite} />)
    expect(mockNewRoomWrite.mock.calls.length).toBe(0)
    fireEvent.click(screen.getByText(/create room/i))
    expect(mockNewRoomWrite.mock.calls.length).toBe(1)
    fireEvent.click(screen.getByText(/create a new room/i))
    expect(mockNewRoomWrite.mock.calls.length).toBe(2)
  })
})

/*

welp... joining and creating rooms will just have to go untested. can't change the url because jest will die

test('creates new room', () => {
  render(<FirebaseContext.Provider value={firebase}>
    <SplashScreen />
  </FirebaseContext.Provider>)
  fireEvent.click(screen.getByText(/create room/i))
})

*/
