import React, { Component } from 'react'
import { Container, Card, Image, Input } from 'semantic-ui-react'

import { setValue } from '../../Firebase/database'
import { onAuth } from '../../Firebase/auth'
import { joinRoom, GAME_STATE } from '../helpers'
import PropTypes from 'prop-types'

class SplashScreenBase extends Component {
  constructor (props) {
    super(props)

    this.state = {
      user: null,
      roomCodeField: '',
      joinLoading: false,
      joinError: ''
    }
  }

  static generateRoomCode () {
    const CHARS = 'bcdfghjklmnpqrstvxz1234567890'
    let ret = ''
    for (let i = 0; i < 6; i++) {
      ret = ret + CHARS.charAt(Math.floor(Math.random() * CHARS.length))
    }

    return ret
  }

  newRoomWrite () {
    const roomCode = SplashScreenBase.generateRoomCode()
    setValue('rooms/' + roomCode, {
      owner: this.state.user,
      creationTime: new Date().getTime(),
      users: {
        [this.state.user]: true
      },
      status: GAME_STATE.LOBBY
    })
    setValue('users/' + this.state.user + '/activeGame', roomCode)

    window.location.pathname = '/room/' + roomCode
  }

  joinRoomOnClick () {
    this.setState({ joinLoading: true })
    joinRoom(
      this.state.roomCodeField,
      this.state.user,
      () => {
        window.location.pathname = '/room/' + this.state.roomCodeField
        this.setState({ joinLoading: false })
      },
      (failure) => this.setState({ joinError: failure, joinLoading: false })
    )
  }

  componentDidMount () {
    onAuth((user) => {
      if (user) {
        this.setState({ user: user.uid })
      } else {
        // No user is signed in.
      }
    })
  }

  render () {
    return (
      <Container>
        <Card.Group centered>
          <NewRoomCard newRoomWrite={this.newRoomWrite.bind(this)} />
          <JoinRoomCard
            joinRoomOnClick={this.joinRoomOnClick.bind(this)}
            roomCode={this.state.roomCodeField}
            updateRoomCode={(_, data) => this.setState({ roomCodeField: data.value })}
            loading={this.state.joinLoading}
            joinError={this.state.joinError}
          />
        </Card.Group>
      </Container>
    )
  }
}

export function JoinRoomCard ({ joinRoomOnClick, roomCode, updateRoomCode, loading, joinError }) {
  return (
    <Card>
      <Image src="https://generative-placeholders.glitch.me/image?width=600&height=300&style=123" wrapped ui={false} />
      <Card.Content>
        <Card.Header>Join Room</Card.Header>
        <Card.Description>
          <Input
            action={{
              content: 'Join',
              onClick: joinRoomOnClick
            }}
            error={Boolean(joinError)}
            placeholder='Room Code'
            value={roomCode}
            onChange={updateRoomCode}
            loading={loading}
          />
        </Card.Description>
      </Card.Content>
    </Card>
  )
}

JoinRoomCard.propTypes = {
  joinRoomOnClick: PropTypes.func,
  roomCode: PropTypes.string,
  updateRoomCode: PropTypes.func,
  loading: PropTypes.bool,
  joinError: PropTypes.bool
}

export function NewRoomCard ({ newRoomWrite = () => {} }) {
  return (
    <Card onClick={newRoomWrite}>
      <Image
        src="https://generative-placeholders.glitch.me/image?width=600&height=300&style=triangles&gap=30"
        wrapped ui={false}
      />
      <Card.Content>
        <Card.Header>Create Room</Card.Header>
        <Card.Description>Create a new room</Card.Description>
      </Card.Content>
    </Card>
  )
}

NewRoomCard.propTypes = {
  newRoomWrite: PropTypes.func
}

const SplashScreen = SplashScreenBase

export default SplashScreen
