import React from 'react'
import { render, screen } from '@testing-library/react'
import App from './App'

test('Nav Bar Renders', () => {
  render(<App />)
  expect(screen.getByText(/home/i)).toBeInTheDocument()
})
