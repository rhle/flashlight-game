import React, { Component } from 'react'
import './App.css'

import { onValueChange } from './components/Firebase/database'
import { signIn, onAuth } from './components/Firebase/auth'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink
} from 'react-router-dom'

import { Menu } from 'semantic-ui-react'
import SplashScreen from './components/Player/SplashScreen'
import Room from './components/Player/Room'
import Archive from './components/Player/Archive'

class AppBase extends Component {
  constructor (props) {
    super(props)

    this.state = {
      user: null,
      activeRoom: null
    }
  }

  componentDidMount () {
    signIn()
    onAuth((user) => {
      if (user) {
        this.setState({ user: user.uid })
        onValueChange('users/' + this.state.user + '/activeGame', (snapshot) => {
          this.setState({ activeRoom: snapshot.val() })
        })
      } else {
        // No user is signed in.
      }
    })
  }

  render () {
    return (
      <Router>
        <Menu>
          <Menu.Item name='home'><NavLink exact to='/'>Home</NavLink></Menu.Item>
          <Menu.Item name='Game'><NavLink to={'/room/' + this.state.activeRoom}>Current Game</NavLink></Menu.Item>
          <Menu.Item name='Archive'><NavLink to={'/archive/'}>Archive</NavLink></Menu.Item>
        </Menu>
        <Switch>
          <Route path="/room/:roomCode"><Room user={this.state.user} isHistory={false} /></Route>
          <Route path="/archive/:roomCode"><Room user={this.state.user} isHistory={true} /></Route>
          <Route path="/archive/"><Archive userId={this.state.user} /></Route>
          <Route path="/"><SplashScreen/></Route>
        </Switch>
      </Router>
    )
  }
}

const App = AppBase

export default App
