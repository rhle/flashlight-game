// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom/extend-expect'

Object.defineProperty(window, 'firebase', {
  writable: false,
  value: {
    auth: jest.fn(() => ({
      onAuthStateChanged: jest.fn(),
      signInAnonymously: jest.fn(() => ({
        catch: jest.fn()
      }))
    })),
    database: jest.fn(() => ({
      ref: jest.fn(() => ({
        set: jest.fn(),
        push: jest.fn(),
        on: jest.fn()
      }))
    }))
  }
})
